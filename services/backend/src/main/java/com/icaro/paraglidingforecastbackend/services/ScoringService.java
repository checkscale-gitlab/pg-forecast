package com.icaro.paraglidingforecastbackend.services;

import com.icaro.paraglidingforecastbackend.models.forecast.Forecast;

public interface ScoringService {
  void scoreForecast(Forecast forecast);
}
