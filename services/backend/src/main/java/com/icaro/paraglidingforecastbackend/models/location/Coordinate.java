package com.icaro.paraglidingforecastbackend.models.location;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.springframework.data.annotation.PersistenceConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
public class Coordinate {
  private final double lat;
  private final double lon;

  @JsonCreator
  @PersistenceConstructor
  public Coordinate(@JsonProperty("lat") double lat, @JsonProperty("lon") double lon) {
    this.lat = lat;
    this.lon = lon;
  }

  public Coordinate(String latlon) {
    String[] latlonSplit = latlon.split(",");
    this.lat = Double.parseDouble(latlonSplit[0]);
    this.lon = Double.parseDouble(latlonSplit[1]);
  }

  public String getLatLon() {
    return String.format("%s,%s", lat, lon);
  }

  @Override
  public String toString() {
    return lat + "," + lon;
  }
}
