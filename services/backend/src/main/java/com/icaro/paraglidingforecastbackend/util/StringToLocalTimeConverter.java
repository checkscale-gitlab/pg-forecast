package com.icaro.paraglidingforecastbackend.util;

import org.joda.time.LocalTime;
import org.springframework.core.convert.converter.Converter;

public class StringToLocalTimeConverter implements Converter<String, LocalTime> {

  @Override
  public LocalTime convert(String source) {
    return new LocalTime(source);
  }
}
