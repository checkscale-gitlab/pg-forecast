package com.icaro.paraglidingforecastbackend.util;

import org.joda.time.LocalDate;
import org.springframework.core.convert.converter.Converter;

public class LocalDateConverter implements Converter<LocalDate, String> {

  @Override
  public String convert(LocalDate source) {
    return source.toString();
  }
}
