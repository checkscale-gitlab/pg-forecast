package com.icaro.paraglidingforecastbackend.models.location;

import lombok.Data;

@Data
public class LocationRules {
  final WindDirectionRules windDirectionRules;
  final WindSpeedRules primaryWindSpeedRules;
  final WindSpeedRules extendedWindSpeedRules;
}
