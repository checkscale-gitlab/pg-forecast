package com.icaro.paraglidingforecastbackend.repositories;

import com.icaro.paraglidingforecastbackend.models.location.Location;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends MongoRepository<Location, String> {}
