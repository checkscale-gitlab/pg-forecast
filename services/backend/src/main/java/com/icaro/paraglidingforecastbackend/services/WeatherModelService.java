package com.icaro.paraglidingforecastbackend.services;

import com.icaro.paraglidingforecastbackend.models.forecast.Forecast;
import com.icaro.paraglidingforecastbackend.models.location.Location;

import java.util.List;

public interface WeatherModelService {
  List<Forecast> getForecasts(List<Location> locations);

  Forecast getForecast(Location location);
}
