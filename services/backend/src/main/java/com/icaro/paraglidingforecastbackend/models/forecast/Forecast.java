package com.icaro.paraglidingforecastbackend.models.forecast;

import com.icaro.paraglidingforecastbackend.models.location.Location;
import lombok.Data;
import org.joda.time.DateTime;

import java.util.List;

@Data
public final class Forecast {
  private final DateTime forecastTimestamp;
  private final Location location;
  private final List<ForecastDay> days;
}
