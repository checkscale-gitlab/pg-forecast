package com.icaro.paraglidingforecastbackend.services;

import com.icaro.paraglidingforecastbackend.models.forecast.Forecast;

import java.util.List;
import java.util.Map;

public interface WeatherForecastService {
  Map<String, List<Forecast>> getForecastsByModel();
  //    Map<String, List<Forecast>> getForecastsByLocation();
}
