package com.icaro.paraglidingforecastbackend.models.location;

import lombok.Getter;

import java.security.InvalidParameterException;

/** Minimum and Maximum Wind Direction in Degrees */
@Getter
public class WindDirectionRules {
  final int primaryDirection;
  final int minimumDirection;
  final int maximumDirection;

  public WindDirectionRules(
      final int primaryDirection, final int minimumDirection, final int maximumDirection) {
    if (minimumDirection >= 360
        || minimumDirection < 0
        || maximumDirection >= 360
        || maximumDirection < 0
        || primaryDirection >= 360
        || primaryDirection < 0) {
      throw new InvalidParameterException("Wind directions must be an integer between 0 and 359");
    }
    this.primaryDirection = primaryDirection;
    this.minimumDirection = minimumDirection;
    this.maximumDirection = maximumDirection;
  }
}
