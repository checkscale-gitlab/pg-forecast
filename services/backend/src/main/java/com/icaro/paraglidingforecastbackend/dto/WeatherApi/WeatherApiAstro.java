package com.icaro.paraglidingforecastbackend.dto.WeatherApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
public class WeatherApiAstro {
  private final LocalTime sunrise;
  private final LocalTime sunset;

  public WeatherApiAstro(
      @JsonProperty("sunrise") final String sunrise, @JsonProperty("sunset") final String sunset) {
    final DateTimeFormatter formatter = DateTimeFormat.forPattern("KK:mm aa"); // 07:52 AM
    this.sunrise = formatter.parseLocalTime(sunrise);
    this.sunset = formatter.parseLocalTime(sunset);
  }
}
