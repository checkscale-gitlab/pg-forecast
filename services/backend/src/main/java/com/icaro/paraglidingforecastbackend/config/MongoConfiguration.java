package com.icaro.paraglidingforecastbackend.config;

import com.icaro.paraglidingforecastbackend.util.LocalDateConverter;
import com.icaro.paraglidingforecastbackend.util.LocalTimeConverter;
import com.icaro.paraglidingforecastbackend.util.StringToLocalDateConverter;
import com.icaro.paraglidingforecastbackend.util.StringToLocalTimeConverter;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Configuration
@ConfigurationProperties(prefix = "spring.data.mongodb")
public class MongoConfiguration extends AbstractMongoClientConfiguration {

    private String uri;

    public void setUri(String uri) {
        // TODO: find a way to use constructor binding
        this.uri = uri;
    }

    public @Bean
    @NotNull
    MongoClient mongoClient() {
        return MongoClients.create(Objects.requireNonNull(uri));
    }

    public @Bean
    MongoTemplate mongoTemplate() {
        return new MongoTemplate(mongoClient(), "data");
    }

    @Override
    protected @NotNull String getDatabaseName() {
        return "data";
    }

    @Override
    public @NotNull MongoCustomConversions customConversions() {
        List<Converter<?, ?>> converters = new ArrayList<>();
        converters.add(new LocalTimeConverter());
        converters.add(new LocalDateConverter());
        converters.add(new StringToLocalDateConverter());
        converters.add(new StringToLocalTimeConverter());
        return new MongoCustomConversions(converters);
    }
}
