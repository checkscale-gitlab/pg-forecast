package com.icaro.paraglidingforecastbackend.dto.VisualCrossing;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.joda.time.LocalTime;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
public class VisualCrossingHour {
  private final LocalTime hour;
  private final Double temp;

  private final Double windSpeed;
  private final Double windGust;
  private final Double windDir;

  private final Double humidity;
  private final Double precipitation;

  private final Double cloudCover;
  private final Double visibility;

  public VisualCrossingHour(
      @JsonProperty("datetime") final String hour,
      @JsonProperty("temp") final Double temp,
      /*@JsonSetter(nulls = Nulls.AS_EMPTY) */ @JsonProperty("humidity") final Double humidity,
      /*@JsonSetter(nulls = Nulls.AS_EMPTY) */ @JsonProperty("precip") final Double precipitation,
      /*@JsonSetter(nulls = Nulls.AS_EMPTY) */ @JsonProperty("windspeed") final Double windSpeed,
      /*@JsonSetter(nulls = Nulls.AS_EMPTY) */ @JsonProperty("windgust") final Double windGust,
      /*@JsonSetter(nulls = Nulls.AS_EMPTY) */ @JsonProperty("winddir") final Double windDir,
      /*@JsonSetter(nulls = Nulls.AS_EMPTY) */ @JsonProperty("cloudcover") final Double cloudCover,
      /*@JsonSetter(nulls = Nulls.AS_EMPTY) */ @JsonProperty("visibility")
          final Double visibility) {

    this.hour = LocalTime.parse(hour);
    this.temp = temp;
    this.humidity = humidity;
    this.precipitation = precipitation;
    this.windGust = windGust;
    this.windSpeed = windSpeed;
    this.windDir = windDir;
    this.cloudCover = cloudCover;
    this.visibility = visibility;
  }
}
