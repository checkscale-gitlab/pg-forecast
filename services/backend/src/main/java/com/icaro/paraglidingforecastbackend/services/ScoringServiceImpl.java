package com.icaro.paraglidingforecastbackend.services;

import com.icaro.paraglidingforecastbackend.models.forecast.Forecast;
import com.icaro.paraglidingforecastbackend.models.forecast.ForecastDay;
import com.icaro.paraglidingforecastbackend.models.forecast.ForecastHour;
import com.icaro.paraglidingforecastbackend.models.location.LocationRules;
import com.icaro.paraglidingforecastbackend.models.location.WindDirectionRules;
import com.icaro.paraglidingforecastbackend.models.location.WindSpeedRules;
import org.joda.time.LocalTime;
import org.springframework.stereotype.Service;

@Service
public class ScoringServiceImpl implements ScoringService {
    private static final Double CLOUD_COVER_LIMIT = 80.0;
    private static final Double CLOUD_COVER_LOWER_LIMIT = 80.0;
    private static final Double PRECIPITATION_LIMIT = 0.2;
    private static final Double PRECIPITATION_LOWER_LIMIT = 0.1;
    private static final Double ZERO = 0.0;
    private static final Double ONE = 1.0;
    private static final double GUST_LIMIT = 12;

    @Override
    public void scoreForecast(final Forecast forecast) {
        for (ForecastDay day : forecast.getDays()) {
            Double highestScore = null;

            for (ForecastHour hour : day.getHours()) {
                Double score;

                score = scoreHour(hour, forecast.getLocation().getLocationRules(), day.getSunrise(), day.getSunset());
                hour.setScore(score);

                if (highestScore == null) {
                    highestScore = score;
                } else if (score != null && score > highestScore) {
                    highestScore = score;
                }

            }

            day.setScore(highestScore);
        }
    }

    private Double scoreHour(final ForecastHour hour, final LocationRules rules, final LocalTime sunrise, final LocalTime sunset) {
        int divider = 1;
        double score = 0;

        try {
            if (hour.getCloudCoverLow() >= CLOUD_COVER_LIMIT
                    || hour.getPrecipitation() >= PRECIPITATION_LIMIT
                    || !directionInRange(hour.getWindDir(), rules.getWindDirectionRules())
                    || !speedInRange(hour.getWindSpeed(), rules.getExtendedWindSpeedRules())
                    || tooGusty(hour.getWindSpeed(), hour.getWindGust())
                    || hour.getHour().isAfter(sunset)
                    || hour.getHour().isBefore(sunrise)
            ) {

                return ZERO;
            } else {

                if (hour.getCloudCoverLow() >= CLOUD_COVER_LOWER_LIMIT) {
                    divider++;
                }

                if (hour.getPrecipitation() >= PRECIPITATION_LOWER_LIMIT) {
                    divider++;
                }

                score += windDirectionScore(hour.getWindDir(), rules.getWindDirectionRules());
                score *=
                        windSpeedScore(
                                hour.getWindSpeed(),
                                rules.getPrimaryWindSpeedRules(),
                                rules.getExtendedWindSpeedRules());

                score /= divider;
                return score;
            }
        } catch (NullPointerException exception) {
            return null;
            // TODO: if logging is added, log this!
        }
    }

    private boolean directionInRange(
            final Double windDir, final WindDirectionRules windDirectionRules) {
        final int minimumDirection = windDirectionRules.getMinimumDirection();
        final int maximumDirection = windDirectionRules.getMaximumDirection();

        if (minimumDirection < maximumDirection) {
            return windDir > minimumDirection && windDir < maximumDirection;
        } else {
            return windDir > minimumDirection || windDir < maximumDirection;
        }
    }

    private boolean speedInRange(final Double windSpeed, final WindSpeedRules windSpeedRules) {
        final int minimumSpeed = windSpeedRules.getMinimumVelocity();
        final int maximumSpeed = windSpeedRules.getMaximumVelocity();

        return windSpeed >= minimumSpeed && windSpeed <= maximumSpeed;
    }

    private boolean tooGusty(final double windSpeed, final double windGust) {
        return windGust - windSpeed >= GUST_LIMIT;
    }

    private Double windDirectionScore(Double windDir, final WindDirectionRules windDirectionRules) {
        final int minimumDirection = windDirectionRules.getMinimumDirection();
        final int maximumDirection = windDirectionRules.getMaximumDirection();
        final int primaryDirection = windDirectionRules.getPrimaryDirection();

        int tempMaxDir = maximumDirection;
        if (minimumDirection > maximumDirection) {
            tempMaxDir += 360;

            if (windDir < minimumDirection) {
                windDir += 360;
            }
        }

        if (windDir <= primaryDirection) {
            return applyHigh((windDir - minimumDirection) / (primaryDirection - minimumDirection));
        } else {
            return applyHigh((tempMaxDir - windDir) / (tempMaxDir - primaryDirection));
        }
    }

    private double applyHigh(double input) {
        return (input - 1) * 0.8 + 1;
    }

    private Double windSpeedScore(
            Double windSpeed,
            final WindSpeedRules primaryWindSpeedRules,
            final WindSpeedRules extendedWindSpeedRules) {
        final int minIdeal = primaryWindSpeedRules.getMinimumVelocity();
        final int maxIdeal = primaryWindSpeedRules.getMaximumVelocity();
        final int minSpeed = extendedWindSpeedRules.getMinimumVelocity();
        final int maxSpeed = extendedWindSpeedRules.getMaximumVelocity();

        if (windSpeed >= minIdeal && windSpeed <= maxIdeal) {
            return ONE;
        }

        if (windSpeed > minIdeal) {
            return applyLow((maxSpeed - windSpeed) / (maxSpeed - maxIdeal));
        } else {
            return applyLow((windSpeed - minSpeed) / (minIdeal - minSpeed));
        }
    }

    private double applyLow(double input) {
        return input * input;
    }
}
