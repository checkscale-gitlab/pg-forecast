package com.icaro.paraglidingforecastbackend.mappers.visualCrossing;

import com.icaro.paraglidingforecastbackend.dto.VisualCrossing.VisualCrossingDto;
import com.icaro.paraglidingforecastbackend.models.forecast.Forecast;
import com.icaro.paraglidingforecastbackend.models.location.Location;
import com.icaro.paraglidingforecastbackend.services.ScoringService;
import org.joda.time.DateTime;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.ERROR,
        uses = {VisualCrossingDayMapper.class})
public abstract class VisualCrossingMapper {

  @Autowired protected ScoringService scoringService;

  public abstract Forecast toForecast(
      DateTime forecastTimestamp, VisualCrossingDto visualCrossingDto, Location location);

  @AfterMapping
  protected void afterMapping(@MappingTarget Forecast forecast) {
    scoringService.scoreForecast(forecast);
  }
}
