# Backend Service

## Table of Contents

- [Overview](#overview)
- [Stack](#stack)
- [Requirements](#requirements)
- [How to Develop](#how-to-develop)
  * [Build the image](#build-the-image)
- [APIs used](#apis-used)

## Overview

This service is responsible for fetching, processing, storing and delivering forecast data.

## Stack

* [Spring](https://spring.io/)
* [Java 17](https://www.java.com/en/)

## Requirements

This service was tested and developed using Java 17 and maven 3.6.

You'll have to modify or add another application.yml file with your API keys.

## How to Develop

Start by installing all dependencies.

```bash
mvn install
```

Then start the service using your IDE.

Note: You should set your `active profile` to local and have the respective application-local.yml file.

Any change you make on the code will be reflected as soon as you save. The site will be available at [http://localhost:3000](http://localhost:3000).

## Build the image

First, you'll need to build the image.

```bash
docker build . -t paragliding-backend-service
```

Then run it.

```bash
docker run -p 8080:8080 --name paragliding-backend -d paragliding-backend-service
```

### APIs Used

Currently, this service uses two free APIs, [Visual Crossing](https://www.visualcrossing.com/), and  [Weather API](https://www.weatherapi.com/).

They are free, and you can create an account to generate your own API keys.