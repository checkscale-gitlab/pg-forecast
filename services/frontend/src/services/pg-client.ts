import axios, {AxiosInstance} from "axios";

const BASE_URL: string = 'http://85.244.239.168:8080/forecast/';
const TIMEOUT: number = 5000;

class PgClient {
    axiosInstance: AxiosInstance;

    constructor() {
        this.axiosInstance = axios.create({
            baseURL: BASE_URL,
            timeout: TIMEOUT,
            headers: {'Content-Type': 'application/json'}
        });
    }

    getAllForecasts = () => this.axiosInstance.get('/all');
}

export enum PgClientStatus {
    notStarted = "notStarted",
    loading = "loading",
    done = "done",
    error = "error"
}

// react context api // useReducer
// redux - redux tools
// custom hook

export default PgClient;