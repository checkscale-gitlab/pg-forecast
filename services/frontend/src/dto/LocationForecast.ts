export interface ModelsForecast { [key: string]: LocationForecast[] }

export interface LocationForecast {
    forecastTimestamp: string,
    location: Location,
    days: ForecastDay[]
}

export interface Location {
    name: string,
    coordinates: {
        lat: number,
        lon: number,
        latLon: string
    },
    picture: string,
    locationType: string,
    locationRules: {
        windDirectionRules: {
            primaryDirection: number
            minimumDirection: number
            maximumDirection: number
        },
        primaryWindSpeedRules: {
            minimumVelocity: number
            maximumVelocity: number
        },
        extendedWindSpeedRules: {
            minimumVelocity: number
            maximumVelocity: number
        },
    }
}

export interface ForecastDay {
    date: string,
    score: number,
    sunrise: string,
    sunset: string,
    hours: ForecastHour[]
}

export interface ForecastHour {
    hour: string,
    score: number,

    temp: number,

    windSpeed: number,
    windGust: number,
    windDir: number,

    precipitation: number,
    humidity: number,

    cloudCoverLow: number,
    cloudCoverMedium: number,
    cloudCoverHigh: number,
    visibility: number,

}