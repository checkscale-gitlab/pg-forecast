import React from "react";
import styles from "./footer.module.css"

const Footer: React.FunctionComponent = () => {
    return (
        <footer className={styles.footerHolder}>

            <div>
                This is a open source project. Check about it&nbsp;
                <a className={styles.repoLink}
                    href={"https://gitlab.com/carlos97h/pg-forecast"}>
                    here
                </a>.
            </div>

        </footer>
    );
}

export default Footer;