import {Location} from "../../../../dto/LocationForecast";
import React from "react";
import styles from "./forecast-location.module.css";

interface IProps {
    location: Location,
    toggle: () => void,
    expanded: boolean,
}

const MISSING_PICTURE = "Image not available";

const ForecastLocation: React.FunctionComponent<IProps> = (props: IProps) => {
    const {
        location,
        toggle,
        expanded,
    } = props;

    return (
        <div className={styles.locationHeader}>

            <div className={styles.locationName}>
                {location.name}
            </div>

            <div className={styles.coordinates}>
                {location.coordinates.lat}, {location.coordinates.lon}
            </div>

            <button className={styles.forecastLocationButton} onClick={toggle}>Details</button>

            {expanded && <div className={styles.imageContainer}>
                <img className={styles.image} src={location.picture} alt={MISSING_PICTURE}/></div>
            }

        </div>);
}

export default ForecastLocation;