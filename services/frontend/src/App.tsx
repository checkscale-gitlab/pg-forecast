import React from 'react';
import ForecastPage from "./components/pages/forecast/forecast-page";
import Header from "./components/header/header";
import Footer from "./components/footer/footer";
import styles from "./app.module.css";

const App: React.FunctionComponent = () => {
    return (
        <div className={styles.siteHolder}>
            <Header/>
            <ForecastPage/>
            <Footer/>
        </div>
    );
}

export default App;
